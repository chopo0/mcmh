/**
 * Automatically generated file. DO NOT MODIFY
 */
package chatmoveleiros.moveleiros.com;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "chatmoveleiros.moveleiros.com";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 39;
  public static final String VERSION_NAME = "1.0.22";
}
